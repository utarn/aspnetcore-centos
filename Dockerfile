FROM centos:7
ENV DOTNET_CLI_TELEMETRY_OPTOUT 1
#RUN rpm --import https://packages.microsoft.com/keys/microsoft.asc
#RUN echo -e "[packages-microsoft-com-prod]\nname=packages-microsoft-com-prod \nbaseurl= https://packages.microsoft.com/yumrepos/microsoft-rhel7.3-prod\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/dotnetdev.repo
RUN rpm -Uvh https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm && yum -y update && yum -y install epel-release sudo
# RUN latest=$(yum search dotnet-runtime | grep 2.1. | tail -1 | cut -f1 -d" ")
#RUN yum  -y install autoconf automake libtool freetype-devel fontconfig libXft-devel libjpeg-turbo-devel libpng-devel giflib-devel libtiff-devel libexif-devel glib2-devel cairo-devel libunwind libicu libgdiplus cabextract xorg-x11-font-utils fontconfig https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm $(yum search dotnet-sdk | grep 2.2 | tail -1 | cut -f1 -d" ") gssntlmssp
RUN yum  -y install autoconf automake libtool freetype-devel fontconfig libXft-devel libjpeg-turbo-devel libpng-devel giflib-devel libtiff-devel libexif-devel glib2-devel cairo-devel libunwind libicu libgdiplus cabextract xorg-x11-font-utils fontconfig https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm dotnet-sdk-2.2 gssntlmssp
RUN yum -y install https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox-0.12.5-1.centos7.x86_64.rpm
# Add NodeJS Support
RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash - && yum install -y nodejs && curl -sL https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo && yum -y install yarn
